process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');

chai.should();
chai.use(chaiHttp);

describe('/GET tweets', () => {
    it('it should GET all tweets for a valid search term', done => {
        chai.request(server)
            .get('/api/search/tweets')
            .query({searchTerm: 'test', searchLimit: 10})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(10);
                done();
            });
    });
    it('it should GET all cached tweets for a valid search term', done => {
        let cachedResponse = null;
        chai.request(server)
            .get('/api/search/tweets')
            .query({searchTerm: 'test', searchLimit: 10})
            .end((err, res) => {
                cachedResponse = res.body;
                chai.request(server)
                    .get('/api/search/tweets')
                    .query({searchTerm: 'test', searchLimit: 10})
                    .end(() => {
                        res.body.should.be.eql(cachedResponse);
                        done();
                    });
            });
    });
    it('it should GET a default of 50 tweets if no search limit is provided', done => {
        chai.request(server)
            .get('/api/search/tweets')
            .query({searchTerm: 'test'})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(50);
                done();
            });
    });
    it('it should throw an error for no search term', done => {
        chai.request(server)
            .get('/api/search/tweets')
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.have.property('code');
                res.body.should.have.property('message');
                done();
            });
    });
});
