const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const RateLimit = require('express-rate-limit');
const serverConfig = require('./config/server.config');
const searchController = require('./controllers/searchController');
const CONSTANTS = require('./config/constants');

const limiter = new RateLimit({
    windowMs: CONSTANTS.QUERY_LIMIT_WINDOW,
    max: CONSTANTS.QUERY_LIMIT_QUERY_MAX,
    delayMs: CONSTANTS.QUERY_LIMIT_DELAY_TIME
});

const app = express();
app.enable('trust proxy');
app.use(limiter);
app.use(morgan('dev'));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.use('/api/search', searchController);

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const server = app.listen(process.env.PORT || serverConfig.port, () => {
    console.log('Server is running');
});

module.exports = server;
