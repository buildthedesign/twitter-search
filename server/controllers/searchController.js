const express = require('express');
const memCache = require('memory-cache');
const Twit = require('twit');

const twitConfig = require('../config/twit.config.js');
const cacheConfig = require('../config/cache.config.js');
const CONSTANTS = require('../config/constants');

const ResponseError = require('../models/ResponseError');

const router = express.Router();
const twitterClient = new Twit(twitConfig);

const processResponse = twitterResponse => {
    const responseData = twitterResponse.resp;
    const resStatusCode = responseData.statusCode;
    if (resStatusCode !== 200) {
        const resErrorMessage = twitterResponse.data.errors[0].message;
        throw new ResponseError(resStatusCode, resErrorMessage);
    }
    return twitterResponse.data.statuses;
};

const validateTransformSearchTerm = searchTerm => {
    if (typeof searchTerm === 'undefined' || searchTerm.trim().length === 0) {
        throw new ResponseError(400, 'Invalid search term');
    }
    return searchTerm.trim();
};

const getTwitterResponse = async (searchTerm, searchLimit) => {
    const cacheKey = `${cacheConfig.name}_${searchTerm}_${searchLimit}`;
    const cacheObject = memCache.get(cacheKey);
    if (cacheObject) {
        return cacheObject;
    }
    let twitterResponse = null;
    await twitterClient.get('search/tweets', {q: searchTerm, count: searchLimit}).then(result => {
        twitterResponse = processResponse(result);
        memCache.put(cacheKey, twitterResponse, cacheConfig.ttl);
    });
    return twitterResponse;
};

router.get('/tweets', async (req, res) => {
    try {
        const searchTerm = validateTransformSearchTerm(req.query.searchTerm);
        const searchLimit = req.query.searchLimit || CONSTANTS.DEFAULT_SEARCH_LIMIT;
        const twitterResponse = await getTwitterResponse(searchTerm, searchLimit);
        res.json(twitterResponse);
    } catch (error) {
        res.status(error.code).json(error);
    }
});

module.exports = router;
