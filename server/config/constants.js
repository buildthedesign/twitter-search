/**
 * We have two choices when it comes to the query rate limit/quota -
 * 1. Deny requests after the rate limit
 * 2. Throttle the response by 'n' every time - Arithmetic progression
 *
 * I'm choosing to throttle the response by 8.9ms on every request.
 */
module.exports = {
    DEFAULT_SEARCH_LIMIT: 50,
    QUERY_LIMIT_WINDOW: 900000,
    QUERY_LIMIT_QUERY_MAX: 450,
    QUERY_LIMIT_DELAY_TIME: 8.9
};
