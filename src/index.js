import './index.sass';

import React from 'react';
import ReactDOM from 'react-dom';

import Landing from './views/landing/landing';

ReactDOM.render(<Landing/>, document.querySelector('main'));
