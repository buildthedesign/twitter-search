import './landing.sass';

import React from 'react';

import Search from '@components/search/search';
import Tweet from '@components/tweet/tweet';

class Landing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tweets: [],
            inFlight: false,
            error: {
                code: null,
                message: null
            }
        };
    }
    handleSearch = searchTerm => {
        /**
         * If already in flight, cancel the previous request
         * Start a new one - do we need to throttle the requests?
         */
        this.setState({
            inFlight: true,
            error: {
                code: null,
                message: null
            }
        });
        fetch(`https://radiant-woodland-72398.herokuapp.com/api/search/tweets?searchTerm=${searchTerm}`).then(response => {
            if (response.ok) {
                return response.json();
            }
            return Promise.reject(response.json());
        })
            .then(response => {
                this.processTweets(response);
            }).catch(error => {
                console.log('caught!');
                console.log(error);
            });
    }
    processTweets = responseTweets => {
        console.log(responseTweets);

        const tweetArray = [];
        responseTweets.map((tweet, index) => {
            tweetArray.push(<Tweet key={index} tweet={tweet}/>);
        });
        this.setState({
            tweets: tweetArray,
            inFlight: false
        });
    }
    componentDidUpdate() {
        $(document).ready(() => {
            $('.collapsible').collapsible({
                onOpen: function(el) {
                    console.log('Open'); return false;
                }
            });
        });
    }
    render() {
        return <div className='landing-container'>
            <Search handleSearch={this.handleSearch}
                inFlight={this.state.inFlight}
                errorMessage={this.state.error.message}></Search>
            {this.state.inFlight && <div className="progress">
                <div className="indeterminate"></div>
            </div>}
            {this.state.tweets}
        </div>;
    }
}

export default Landing;
