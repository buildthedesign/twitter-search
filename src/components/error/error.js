import './error.sass';

import React from 'react';

class Error extends React.Component {
    render() {
        return <p className="error">{this.props.message}</p>
    }
}

export default Error;
