import './content.sass';

import React from 'react';

class Content extends React.Component {
    render() {
        const twitterUserMentions = this.props.details.entities.user_mentions;
        const userMentions = [];
        twitterUserMentions.map((tweetMention, index) => {
            const userUrl = `http://www.twitter.com/${tweetMention.screen_name}`;
            userMentions.push(
                <p key={index}>
                    <span>{tweetMention.name} - </span>
                    <a href={userUrl}>{tweetMention.screen_name}</a>
                </p>);
        });
        return <div className='content'>
            {userMentions.length > 0 &&
                <div>
                    <p className='content-header'>User Mentions -</p>
                    {userMentions}
                </div>
            }
        </div>;
    }
}

export default Content;
