import './search.sass';

import React from 'react';
import classNames from 'classnames';

import Error from '@components/error/error';

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            validSearchTerm: false,
            searchTerm: null,
            activeSearchTerm: null
        };
    }
    isSearchTermValid = searchTerm => {
        const isTermValid = searchTerm !== '' && searchTerm.length > 2;
        return isTermValid;
    }
    handleSearchTermChange = event => {
        const searchTerm = event.target.value.trim();
        this.setState({
            searchTerm: searchTerm,
            validSearchTerm: this.isSearchTermValid(searchTerm)
        });
    }
    handleSearch = event => {
        if (this.state.activeSearchTerm !== this.state.searchTerm && this.state.validSearchTerm) {
            this.setState({
                activeSearchTerm: this.state.searchTerm
            });
            this.props.handleSearch(this.state.searchTerm);
        }
    }
    render() {
        const searchButtonClasses = classNames('waves-effect waves-light blue btn search-button',
            {disabled: !this.state.validSearchTerm || this.props.inFlight});
        return <div>
            <section className='input-field search-container'>
                <a className={searchButtonClasses} onClick={this.handleSearch}>Search</a>
                <input placeholder="Enter a search term" className="search-input" id="searchInput" type="text" required onChange={this.handleSearchTermChange}></input>
            </section>
            {this.state.activeSearchTerm &&
                <p className="search-message">Search results for <span className="search-term">{this.state.activeSearchTerm}</span></p>
            }
            {this.props.errorMessage && <Error message={this.props.errorMessage}/>}
        </div>;
    }
}

export default Search;
