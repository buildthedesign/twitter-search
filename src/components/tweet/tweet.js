import './tweet.sass';

import React from 'react';

import User from '@components/user/user';
import Accordion from '@components/accordion/accordion';

class Tweet extends React.Component {
    render() {
        return <section className='tweet-container'>
            <User user={this.props.tweet.user}/>
            <Accordion header={this.props.tweet.text} details={this.props.tweet}/>
        </section>;
    }
}

export default Tweet;
