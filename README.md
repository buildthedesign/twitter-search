# Twitter Search

There are two projects to this application -
1. Server - the backend which talks to twitter
1. Client - the frontend which consumes the data and renders the tweets

## Server

### Commands
- Test - ```npm test```
- Server Start (after ```npm install```) - ```npm start```
- Hosted at https://radiant-woodland-72398.herokuapp.com/api/search/tweets
    - Parameters:
        - searchTerm - Search Term
        - searchLimit - # of searches to be fetched
    - Example: https://radiant-woodland-72398.herokuapp.com/api/search/tweets?searchTerm=test&searchLimit=10

### Design
- Configuration driven (server/config)
    - cache
        - name
        - time to live
    - twitter
        - credentials/tokens
- Throttling mechanism
    - Each request is cached for 5 seconds
    - Each request is throttled by ~9ms to ensure that the application meets the twitter 450 req/15 min quota

### Possible Improvements
- Throttling should be done at the network level (twitter fetch) and not at the request (/get) level. This is so that cached responses are not throttled.
- Better tests
- Security & production performance

## Client

### Commands
- Test - ```npm test```
- Build - ```npm run build```
- Webpack dev server start (after ```npm install```) - ```npm devstart```
- Hosted at https://twitter-search-client.herokuapp.com/

### Design
- Search is enabled only if the search term is > 2 characters
- Search is conducted only if the search term or the # of requested search results has changed from the previous search
- The user can click on the hash tags and the user mentions
- The user can click on the accordion header to see more details

### Possible Improvements
- UI - More tweet details
- Better tests
- Security & production performance