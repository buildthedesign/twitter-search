import './landing.sass';

import React from 'react';

import Search from '@components/search/search';
import Tweet from '@components/tweet/tweet';

class Landing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tweets: [],
            inFlight: false,
            error: {
                code: null,
                message: null
            }
        };
    }
    handleSearch = (searchTerm, numOfResultsSelectValue) => {
        this.setState({
            inFlight: true,
            error: {
                code: null,
                message: null
            }
        });
        let twitterURL = 'https://radiant-woodland-72398.herokuapp.com/api/search/tweets';
        twitterURL = `${twitterURL}?searchTerm=${encodeURIComponent(searchTerm)}&searchLimit=${numOfResultsSelectValue}`;
        fetch(twitterURL).then(response => {
            if (response.ok) {
                return response.json();
            }
            throw response;
        }).then(response => {
            this.processTweets(response);
        }).catch(error => {
            error.json().then(errorObject => {
                this.setState({
                    inFlight: false,
                    error: {
                        code: errorObject.code,
                        message: errorObject.message
                    }
                });
            });
        });
    }
    processTweets = responseTweets => {
        const tweetArray = [];
        responseTweets.map((tweet, index) => {
            tweetArray.push(<Tweet key={index} tweet={tweet}/>);
        });
        this.setState({
            tweets: tweetArray,
            inFlight: false
        });
    }
    componentDidUpdate() {
        $('.collapsible').collapsible();
    }
    render() {
        return <div className='landing-container'>
            <Search handleSearch={this.handleSearch} inFlight={this.state.inFlight}
                errorMessage={this.state.error.message}></Search>
            {this.state.inFlight && <div className="progress">
                <div className="indeterminate"></div>
            </div>}
            {this.state.tweets}
        </div>;
    }
}

export default Landing;
