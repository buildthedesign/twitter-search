import './user.sass';

import React from 'react';

import Image from './image/image';

class User extends React.Component {
    render() {
        return <div className='user'>
            <Image src={this.props.user.profile_image_url}/>
            <p className='name'>{this.props.user.name}</p>
        </div>;
    }
}
export default User;
