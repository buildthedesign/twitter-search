import './image.sass';

import React from 'react';

class Image extends React.Component {
    render() {
        return <img className='user-image' src={this.props.src} />;
    }
}

export default Image;
