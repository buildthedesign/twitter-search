import './header.sass';

import React from 'react';
import * as linkify from 'linkifyjs';
import linkifyHtml from 'linkifyjs/html';
import hashtag from 'linkifyjs/plugins/hashtag';
import mention from 'linkifyjs/plugins/mention';

hashtag(linkify);
mention(linkify);

const linkifyOptions = {
    defaultProtocol: 'https',
    formatHref: function(href, type) {
        if (type === 'hashtag') {
            href = `https://twitter.com/hashtag/${href.substring(1)}`;
        }
        if (type === 'mention') {
            href = `https://twitter.com/${href.substring(1)}`;
        }
        return href;
    }
};

class Header extends React.Component {
    linkifyText = text => ({__html: linkifyHtml(text, linkifyOptions)});
    componentDidMount() {
        if (this.refs.accordionHeader) {
            const anchorTags = this.refs.accordionHeader.querySelectorAll('.collapsible-header a');
            for (let count = 0; count < anchorTags.length; count++) {
                const element = anchorTags[count];
                element.addEventListener('click', evt => {
                    evt.stopPropagation();
                });
            }
        }
    }
    render() {
        return <div ref='accordionHeader' className="collapsible-header">
            <h5 dangerouslySetInnerHTML={this.linkifyText(this.props.header)}/>
        </div>;
    }
}

export default Header;
