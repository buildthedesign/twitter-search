import './accordion.sass';

import React from 'react';

import Content from './content/content';
import Header from './header/header';

class Accordion extends React.Component {
    render() {
        return <ul className="collapsible" data-collapsible="expandable">
            <li>
                <Header header={this.props.header}/>
                <div className="collapsible-body"><Content details={this.props.details}></Content></div>
            </li>
        </ul>;
    }
}

export default Accordion;
