const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve('dist'),
        filename: 'index.bundle.js'
    },
    resolve: {
        alias: {
            '@components': path.resolve('./src', './components'),
            '@views': path.resolve('./src', './views')
        }
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader'
            }]
        }, {
            test: /\.sass$/,
            use: ExtractTextWebpackPlugin.extract({
                use: [{
                    loader: 'css-loader',
                    options: {
                        importLoaders: 2,
                        url: false
                    }
                }, {
                    loader: 'sass-loader'
                }]
            })
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve('./src', 'index.html'),
            filename: path.resolve('./dist', 'index.html'),
            inject: 'body',
            cache: true,
            hash: true
        }),
        new ExtractTextWebpackPlugin('index.css')
    ]
};
