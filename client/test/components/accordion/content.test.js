import React from 'react';

import renderer from 'react-test-renderer';

import Content from '../../../src/components/accordion/content/content';

test('accordion content should match the snapshot', () => {
    const twitterJSON = {
        entities: {
            user_mentions: [{
                screen_name: 'GB',
                name: 'Gautam Bhutani'
            }]
        }
    };
    const component = renderer.create(
        <Content details={twitterJSON}/>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
