import React from 'react';

import renderer from 'react-test-renderer';

import Header from '../../../src/components/accordion/header/header';

test('accordion header should match the snapshot', () => {
    const component = renderer.create(
        <Header header='#testHashtag @testUserName test normal text'/>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
